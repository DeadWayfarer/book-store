package main;

import entities.User;
import gui.forms.*;
import javafx.concurrent.Task;
import org.hibernate.CacheMode;
import utils.HibernateUtil;

/**
 * Главный класс программы
 * @author Teacher
 */
public class Main {
    
    public static String appName = "BookStore";
    
    public static User currentUser;
    
    public static LoginForm loginForm;
    public static RegisterForm registerForm;
    
    public static ManagerForm managerForm;
    public static AuthorListForm authorListForm;
    public static GenreListForm genreListForm;
    public static PublisherListForm publisherListForm;
    public static BookListForm bookListForm;
    
    public static CustomerForm customerForm;

    public static void main(String[] args) {
        HibernateUtil.init();
        loginForm = new LoginForm();
    }
    
}
