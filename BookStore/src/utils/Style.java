package utils;

import java.awt.Color;

/**
 * Класс для работы со стилями
 *
 * @author Teacher
 */
public class Style {

    
    
    static public final Color C_BLUE = new Color(0x04A0FF);
    static public final Color C_GREEN = new Color(0x00D9BB);
    static public final Color C_RED = new Color(0xFF4A6D);
    static public final Color C_ORANGE = new Color(0xFF9C1A);
    static public final Color C_GRAY = new Color(0xF7F9F9);
    
    static public final Color C_BACKGROUND = C_GRAY;
}
