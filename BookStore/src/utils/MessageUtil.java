package utils;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Класс для работы с сообщениями
 *
 * @author Teacher
 */
public class MessageUtil {

    public static final int INFO = JOptionPane.INFORMATION_MESSAGE;
    public static final int ASK = JOptionPane.QUESTION_MESSAGE;
    
    public static void message(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Сообщение", INFO);
    }
    
    public static boolean confirm(String msg) {
        String[] options = "Да,Нет".split("[,]");
        return JOptionPane.showOptionDialog(null, msg, "Подтверждение", 0, ASK, null, options, null) == 0;
    }

    public static void error(Throwable t, String msg) {
        error(t);
        message(msg);
    }
    
    public static void error(Throwable t) {
        Logger.getGlobal().log(Level.SEVERE, "ОШИБКА!", t);
    }
}
