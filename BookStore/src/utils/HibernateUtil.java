package utils;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import static utils.MessageUtil.*;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.SessionFactory;

/**
 * Класс для работы с Hibernate
 *
 * @author Teacher
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;
    private static Session appSession;

    /**
     * Инициализация Hibernate
     */
    public static void init() {
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
            appSession = openSession();
        } catch (Throwable ex) {
            error(ex,"Не удалось подключиться к БД!");
        }
    }
    
    /**
     * Завершает работу с Hibernate
     */
    public static void stop() {
        try {
            appSession.close();
        }
        catch(Exception e) {
            error(e);
        }
        try {
            sessionFactory.close();
        }
        catch(Exception e) {
            error(e);
        }
    }
    
    /**
     * Открывает и возвращает новую сессию
     * @return 
     */
    public static Session openSession() {
        return sessionFactory.openSession();
    }
    
    /**
     * Возвращает сущность по HQL запросу
     * 
     * @param query - HQL запрос
     */
    public static Object getEntity(String query) {
        query = "FROM "+query;
        try {
            return appSession.createQuery(query).uniqueResult();
        }
        catch (Throwable t) {
            error(t);
        }
        return null;
    }
    
    /**
     * Возвращает сущности по HQL запросу
     * 
     * @param query - HQL запрос
     */
    public static List getEntities(String query) {
        query = "FROM "+query;
        try {
            return appSession.createQuery(query).list();
        }
        catch (Throwable t) {
            error(t);
        }
        return null;
    }
    
    /**
     * Сохраняет перечень сущностей
     * 
     * @param entities - перечень сущностей
     * @return Возвращает true если успешно сохранено, иначе false
     */
    public static boolean saveEntities(Object... entities) {
        try {
            for (Object entity : entities) {
                appSession.beginTransaction();
                appSession.saveOrUpdate(entity);
                appSession.getTransaction().commit();
            }
        }
        catch (Throwable t) {
            error(t);
            return false;
        }
        return true;
    }
    
    /**
     * Удаляет перечень сущностей
     * 
     * @param entities - перечень сущностей
     * @return Возвращает true если успешно удалено, иначе false
     */
    public static boolean deleteEntities(Object... entities) {
        try {
            for (Object entity : entities) {
                appSession.beginTransaction();
                appSession.delete(entity);
                appSession.getTransaction().commit();
            }
        }
        catch (Throwable t) {
            error(t);
            return false;
        }
        return true;
    }
    
    /**
     * J,yjdkztn перечень сущностей
     * 
     * @param entities - перечень сущностей
     * @return Возвращает true если успешно обновлено, иначе false
     */
    public static boolean refreshEntities(Object... entities) {
        try {
            for (Object entity : entities) {
                appSession.beginTransaction();
                appSession.refresh(entity);
                appSession.getTransaction().commit();
            }
        }
        catch (Throwable t) {
            error(t);
            return false;
        }
        return true;
    }
    
    public static List unproxyItems(List items) {
        ArrayList unproxiedItems = new ArrayList();
        for (Object item : items) {
            unproxiedItems.add(Hibernate.unproxy(item));
        }
        return unproxiedItems;
    }
    
    public static Session getAppSession() {
        return appSession;
    }
}
