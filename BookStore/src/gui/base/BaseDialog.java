package gui.base;

import static main.Main.appName;
import static utils.Style.C_BACKGROUND;

/**
 * Базовый диалог
 * @author Teacher
 */
public abstract class BaseDialog extends javax.swing.JDialog {

    private boolean commited = false;
    
    public BaseDialog() {
        super((java.awt.Frame) null, true);
        initComponents();
    }
    
    /**
     * Вызывается после инициализации в конструкторе
     */
    abstract protected void afterInit();
    
    protected void commit() {
        commited = true;
        dispose();
    }
    
    public boolean isCommited() {
        return commited;
    }
    
    /**
     * Устанавливает настройки приложения
     */
    private void setSettings() {
        getContentPane().setBackground(C_BACKGROUND);
        setBackground(C_BACKGROUND);
        setTitle(appName);
    }

    /**
     * Сгенерированный контент
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
