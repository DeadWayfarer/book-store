package gui.base;

import static main.Main.*;
import utils.HibernateUtil;
import static utils.HibernateUtil.*;
import static utils.MessageUtil.*;
import static utils.Style.*;

abstract public class BaseForm extends javax.swing.JFrame {

    private BaseForm lastForm;
    
    public BaseForm(BaseForm lastForm) {
        this.lastForm = lastForm;
        initComponents();
        if (lastForm == null) {
            backBtn.setVisible(false);
        }
        else {
            lastForm.setVisible(false);
        }
        setSettings();
        setVisible(true);
    }
    
    /**
     * Вызывается после инициализации в конструкторе
     */
    abstract protected void afterInit();
    
    /**
     * Обновляет содержимое интерфейса
     */
    abstract protected void updateUI();
    
    /**
     * Устанавливает настройки приложения
     */
    private void setSettings() {
        getContentPane().setBackground(C_BACKGROUND);
        setTitle(appName);
        header.setText(appName);
    }
    
    /**
     * Возвращается на предыдущую форму
     */
    protected void goBack() {
        lastForm.setVisible(true);
        dispose();
    }

    /**
     * Сгенерированный контент
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headerPanel = new javax.swing.JPanel();
        backBtn = new javax.swing.JButton();
        header = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        headerPanel.setBackground(C_BACKGROUND);
        headerPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        backBtn.setBackground(C_BLUE);
        backBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backBtn.setForeground(C_GRAY);
        backBtn.setText("Назад");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        headerPanel.add(backBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        header.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        header.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        header.setText("Заголовок");
        headerPanel.add(header, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 11, 400, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(headerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 255, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        goBack();
    }//GEN-LAST:event_backBtnActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        HibernateUtil.stop();
    }//GEN-LAST:event_formWindowClosing

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        headerPanel.setSize(getWidth(), headerPanel.getHeight());
        header.setSize(getWidth(), header.getHeight());
    }//GEN-LAST:event_formComponentResized

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    protected javax.swing.JLabel header;
    private javax.swing.JPanel headerPanel;
    // End of variables declaration//GEN-END:variables
}
