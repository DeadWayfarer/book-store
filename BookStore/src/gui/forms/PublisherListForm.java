package gui.forms;

import gui.base.*;
import static main.Main.*;
import entities.*;
import gui.dialogs.AddEditPublisherDialog;
import static utils.HibernateUtil.*;
import static utils.MessageUtil.*;
import static utils.Style.*;

/**
 * Форма список издателей
 *
 * @author Teacher
 */
public class PublisherListForm extends BaseForm {
    
    public PublisherListForm(BaseForm lastForm) {
        super(lastForm);
        initComponents();
        afterInit();
    }
    
    public PublisherListForm() {
        super(null);
        initComponents();
        afterInit();
    }

    /**
     * Сгенерированный контент
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headerPanel = new javax.swing.JPanel();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        publisherListView = new gui.components.ListView();
        addBtn = new javax.swing.JButton();
        editBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        headerPanel.setBackground(C_BACKGROUND);

        javax.swing.GroupLayout headerPanelLayout = new javax.swing.GroupLayout(headerPanel);
        headerPanel.setLayout(headerPanelLayout);
        headerPanelLayout.setHorizontalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        headerPanelLayout.setVerticalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 45, Short.MAX_VALUE)
        );

        bodyPanel.setBackground(C_BACKGROUND);

        publisherListView.setBackground(C_BACKGROUND);
        publisherListView.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        publisherListView.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                publisherListViewValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(publisherListView);

        addBtn.setBackground(C_BLUE);
        addBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        addBtn.setForeground(C_GRAY);
        addBtn.setText("Добавить");
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });

        editBtn.setBackground(C_ORANGE);
        editBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        editBtn.setForeground(C_GRAY);
        editBtn.setText("Изменить");
        editBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editBtnActionPerformed(evt);
            }
        });

        deleteBtn.setBackground(C_RED);
        deleteBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        deleteBtn.setForeground(C_GRAY);
        deleteBtn.setText("Удалить");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(bodyPanelLayout.createSequentialGroup()
                        .addComponent(addBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(editBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(deleteBtn)
                        .addGap(0, 59, Short.MAX_VALUE)))
                .addContainerGap())
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addBtn)
                    .addComponent(editBtn)
                    .addComponent(deleteBtn))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(headerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void publisherListViewValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_publisherListViewValueChanged
        deleteBtn.setEnabled(true);
        editBtn.setEnabled(true);
    }//GEN-LAST:event_publisherListViewValueChanged

    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBtnActionPerformed
        Publisher publisher = new Publisher();

        BaseDialog dialog = new AddEditPublisherDialog(publisher);
        dialog.setVisible(true);
        if (dialog.isCommited()) {
            if (saveEntities(publisher)) {
                message("Успешно сохранено!");
                updateUI();
            }
            else {
                message("Не удалось сохранить!");
            }
        }
    }//GEN-LAST:event_addBtnActionPerformed

    private void editBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editBtnActionPerformed
        Publisher publisher = (Publisher) publisherListView.getSelectedValue();

        BaseDialog dialog = new AddEditPublisherDialog(publisher);
        dialog.setVisible(true);
        if (dialog.isCommited()) {
            if (saveEntities(publisher)) {
                message("Успешно сохранено!");
                updateUI();
            }
            else {
                message("Не удалось сохранить!");
            }
        }
    }//GEN-LAST:event_editBtnActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        Publisher publisher = (Publisher) publisherListView.getSelectedValue();

        if (!confirm("Вы действительно хотите удалить издателя?")) {
            return;
        }

        if (deleteEntities(publisher)) {
            message("Успешно удален!");
            updateUI();
        }
        else {
            message("Не удалось удалить!");
        }
    }//GEN-LAST:event_deleteBtnActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addBtn;
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JButton editBtn;
    private javax.swing.JPanel headerPanel;
    private javax.swing.JScrollPane jScrollPane2;
    private gui.components.ListView publisherListView;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void afterInit() {
        setLocationRelativeTo(null);
        updateUI();
    }
    
    @Override
    protected void updateUI() {
        publisherListView.load(Publisher.class.getSimpleName());
        
        deleteBtn.setEnabled(false);
        editBtn.setEnabled(false);
    }
}
