package gui.components;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.sql.Date;
import javax.swing.JTextField;
import static utils.Style.*;
import static utils.MessageUtil.*;

/**
 * Поле для ввода с поддержкой валидации
 * @author Teacher
 */
public class InputField extends JTextField {

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    
    public static final String RULE_EMAIL = "RULE_EMAIL";
    public static final String RULE_FLOAT = "RULE_FLOAT";
    
    public void error(String msg) {
        setBackground(C_RED);
        message(msg);
        throw new RuntimeException(msg);
    }

    public void validate(String rule) {
        setBackground(C_BACKGROUND);
        String value = getText();

        String msgStart = "Данное поле должно содержать ";
        
        switch (rule) {
            case RULE_EMAIL:
                if (!value.matches("\\w+@\\w+[.]\\w+")) {
                    error(msgStart+"Email! Например example@mail.com");
                }
                break;
            case RULE_FLOAT:
                if (!value.matches("\\d+[,.]?\\d*")) {
                    error(msgStart+"число!");
                }
                break;
        }
    }

    public void required() {
        setBackground(C_BACKGROUND);
        if (getText().isEmpty()) {
            error("Поле обязательно для заполнения!");
        }
    }
    
    public Date getDate() {
        if (getText().isEmpty()) {
            return null;
        }
        try {
            LocalDate date = LocalDate.parse(getText(),DATE_FORMATTER);
            return Date.valueOf(date);
        }
        catch (Exception e) {
            error("Не правильная дата! Нужный формат: ДД.ММ.ГГГГ!");
        }
        return null;
    }
    
    public void setDate(java.util.Date val) {
        if (val == null) {
            setText("");
            return;
        }
        Date date = (Date) val;
        LocalDate localDate = date.toLocalDate();
        setText(DATE_FORMATTER.format(localDate));
    }
    
    public Float getFloat() {
        if (getText().isEmpty()) {
            return null;
        }
        validate(RULE_FLOAT);
        return Float.valueOf(getText().replace(",", "."));
    }
    
    public void setFloat(Float val) {
        if (val == null) {
            setText("");
        }
        
        setText(val.toString());
    }
}
