/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.components;

import entities.Author;
import entities.Book;
import entities.Genre;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 * TableView для отображения сущности Book
 * @author Teacher
 * @param <T>
 */
public class BookTableView<T extends Book> extends TableView<T> {

    @Override
    void defineColumns(DefaultTableModel model) {
        model.addColumn("Название");
        model.addColumn("Описание");
        model.addColumn("ISBN");
        model.addColumn("Издатель");
        model.addColumn("Стоимост");
        model.addColumn("Авторы");
        model.addColumn("Жанры");
    }

    @Override
    void fillRow(ArrayList row, T item) {
        row.add(item.getName());
        row.add(item.getDescription());
        row.add(item.getIsbn());
        row.add(item.getPublisher());
        row.add(item.getCost());
        
        String authors = "";
        String genres = "";
        for (Author author : item.getAuthors()) {
            if (!authors.isEmpty()) {
                authors += ", ";
            }
            authors += author.getFirstName();
            authors += " " + author.getLastName();
            authors += " " + author.getMiddleName();
        }
        
        for (Genre genre : item.getGenres()) {
            if (!genres.isEmpty()) {
                genres += ", ";
            }
            genres += genre.getName();
        }
        
        row.add(authors);
        row.add(genres);
    }
    
}
