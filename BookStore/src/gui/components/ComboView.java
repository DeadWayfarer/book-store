package gui.components;

import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import utils.HibernateUtil;

/**
 * JComboBox с загрузкой сущностей
 * @author Teacher
 */
public class ComboView extends JComboBox<Object>{
    public void load(String query) {
       List items = HibernateUtil.getEntities(query);
       items = HibernateUtil.unproxyItems(items);
       load(items);
   } 
   
   public void load (List items) {
       DefaultComboBoxModel model = new DefaultComboBoxModel();
       for (Object item : items) {
           model.addElement(item);
       }
       setModel(model);
   }
}
