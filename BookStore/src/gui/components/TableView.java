package gui.components;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import utils.HibernateUtil;

/**
 * JTable с загрузкой сущностей
 * @author Teacher
 */
abstract public class TableView<T> extends JTable {
    private List<T> items;
    private DefaultTableModel model;
    
    public TableView() {
        model = new DefaultTableModel();
        defineColumns(model);
        
        setModel(model);
    }
    
    public T getSelectedItem() {
        int i = getSelectedRow();
        if (i == -1) {
            return null;
        }
        return items.get(i);
    }
    
    abstract void fillRow(ArrayList row, T item);
    abstract void defineColumns(DefaultTableModel model);
    
    public void load(String query) {
        items = HibernateUtil.getEntities(query);
        load(items);
    }
    
    public void load(List<T> items) {
        model.setRowCount(0);
        
        for (T item : items) {
            ArrayList row = new ArrayList<>();
            fillRow(row, item);
            model.addRow(row.toArray());
        }
        
        setModel(model);
    }
}
