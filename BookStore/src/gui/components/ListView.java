package gui.components;

import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import utils.HibernateUtil;

/**
 * JList с загрузкой сущностей
 *
 * @author Teacher
 */
public class ListView extends JList<Object> {

    public void load(String query) {
        List items = HibernateUtil.getEntities(query);
        load(items);
    }

    public void load(List items) {
        DefaultListModel model = new DefaultListModel();
        for (Object item : items) {
            model.addElement(item);
        }
        setModel(model);
    }
}
