package gui.dialogs;

import entities.Author;
import gui.base.BaseDialog;
import java.sql.Date;
import static main.Main.*;
import utils.HibernateUtil;
import static utils.HibernateUtil.*;
import static utils.MessageUtil.*;
import static utils.Style.*;

/**
 * Диалог для редактирования сущности Author
 * @author Teacher
 */
public class AddEditAuthorDialog extends BaseDialog {

    Author author;
    
    public AddEditAuthorDialog(Author author) {
        super();
        initComponents();
        this.author = author;
        afterInit();
    }

    /**
     * Сгенерированный контент
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fnamePane = new javax.swing.JPanel();
        fnameField = new gui.components.InputField();
        lnamePanel = new javax.swing.JPanel();
        lnameField = new gui.components.InputField();
        mnamePanel = new javax.swing.JPanel();
        mnameField = new gui.components.InputField();
        birthPanel = new javax.swing.JPanel();
        birthField = new gui.components.InputField();
        deathPanel = new javax.swing.JPanel();
        deathField = new gui.components.InputField();
        SuccessBtn = new javax.swing.JButton();
        CommonBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        fnamePane.setBackground(C_BACKGROUND);
        fnamePane.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Имя:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        fnameField.setBackground(C_GRAY);
        fnameField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout fnamePaneLayout = new javax.swing.GroupLayout(fnamePane);
        fnamePane.setLayout(fnamePaneLayout);
        fnamePaneLayout.setHorizontalGroup(
            fnamePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fnameField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        fnamePaneLayout.setVerticalGroup(
            fnamePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fnameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        lnamePanel.setBackground(C_BACKGROUND);
        lnamePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Фамилия:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        lnameField.setBackground(C_GRAY);
        lnameField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout lnamePanelLayout = new javax.swing.GroupLayout(lnamePanel);
        lnamePanel.setLayout(lnamePanelLayout);
        lnamePanelLayout.setHorizontalGroup(
            lnamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lnameField, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
        );
        lnamePanelLayout.setVerticalGroup(
            lnamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lnameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        mnamePanel.setBackground(C_BACKGROUND);
        mnamePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Отчество:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        mnameField.setBackground(C_GRAY);
        mnameField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout mnamePanelLayout = new javax.swing.GroupLayout(mnamePanel);
        mnamePanel.setLayout(mnamePanelLayout);
        mnamePanelLayout.setHorizontalGroup(
            mnamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mnameField, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
        );
        mnamePanelLayout.setVerticalGroup(
            mnamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mnameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        birthPanel.setBackground(C_BACKGROUND);
        birthPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Дата рождения:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        birthField.setBackground(C_GRAY);
        birthField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout birthPanelLayout = new javax.swing.GroupLayout(birthPanel);
        birthPanel.setLayout(birthPanelLayout);
        birthPanelLayout.setHorizontalGroup(
            birthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(birthField, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
        );
        birthPanelLayout.setVerticalGroup(
            birthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(birthField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        deathPanel.setBackground(C_BACKGROUND);
        deathPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Дата смерти:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        deathField.setBackground(C_GRAY);
        deathField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout deathPanelLayout = new javax.swing.GroupLayout(deathPanel);
        deathPanel.setLayout(deathPanelLayout);
        deathPanelLayout.setHorizontalGroup(
            deathPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(deathField, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
        );
        deathPanelLayout.setVerticalGroup(
            deathPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(deathField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        SuccessBtn.setBackground(C_GREEN);
        SuccessBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SuccessBtn.setForeground(C_GRAY);
        SuccessBtn.setText("Сохранить");
        SuccessBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SuccessBtnActionPerformed(evt);
            }
        });

        CommonBtn.setBackground(C_BACKGROUND);
        CommonBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        CommonBtn.setText("Отмена");
        CommonBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommonBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fnamePane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lnamePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mnamePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(birthPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deathPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(SuccessBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CommonBtn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fnamePane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lnamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mnamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(birthPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deathPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SuccessBtn)
                    .addComponent(CommonBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CommonBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CommonBtnActionPerformed
        dispose();
    }//GEN-LAST:event_CommonBtnActionPerformed

    private void SuccessBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SuccessBtnActionPerformed
        fnameField.required();
        lnameField.required();
        mnameField.required();
        birthField.required();
        
        Date birth = birthField.getDate();
        Date death = deathField.getDate();
        
        author.setFirstName(fnameField.getText());
        author.setLastName(lnameField.getText());
        author.setMiddleName(mnameField.getText());
        author.setDateOfBirth(birth);
        author.setDateOfDeath(death);
        
        commit();
    }//GEN-LAST:event_SuccessBtnActionPerformed

    @Override
    protected void afterInit() {
        setLocationRelativeTo(null);
        getContentPane().setBackground(C_BACKGROUND);
        
        fnameField.setText(author.getFirstName());
        lnameField.setText(author.getLastName());
        mnameField.setText(author.getMiddleName());
        birthField.setDate(author.getDateOfBirth());
        deathField.setDate(author.getDateOfDeath());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CommonBtn;
    private javax.swing.JButton SuccessBtn;
    private gui.components.InputField birthField;
    private javax.swing.JPanel birthPanel;
    private gui.components.InputField deathField;
    private javax.swing.JPanel deathPanel;
    private gui.components.InputField fnameField;
    private javax.swing.JPanel fnamePane;
    private gui.components.InputField lnameField;
    private javax.swing.JPanel lnamePanel;
    private gui.components.InputField mnameField;
    private javax.swing.JPanel mnamePanel;
    // End of variables declaration//GEN-END:variables
}
