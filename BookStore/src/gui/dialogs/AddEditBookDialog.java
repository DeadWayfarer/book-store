package gui.dialogs;

import entities.Author;
import entities.Book;
import entities.Genre;
import entities.Publisher;
import gui.base.BaseDialog;
import java.util.ArrayList;
import static main.Main.*;
import utils.HibernateUtil;
import static utils.HibernateUtil.*;
import static utils.MessageUtil.*;
import static utils.Style.*;

/**
 * Диалог для редактирования сущности Book
 * @author Teacher
 */
public class AddEditBookDialog extends BaseDialog {

    private Book book;
    private ArrayList<Author> authors;
    private ArrayList<Genre> genres;
    
    public AddEditBookDialog(Book book) {
        super();
        initComponents();
        this.book = book;
        afterInit();
    }

    /**
     * Сгенерированный контент
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CommonBtn = new javax.swing.JButton();
        SuccessBtn = new javax.swing.JButton();
        namePanel = new javax.swing.JPanel();
        nameField = new gui.components.InputField();
        descPanel = new javax.swing.JPanel();
        descField = new gui.components.InputField();
        ISBNPanel = new javax.swing.JPanel();
        ISBNField = new gui.components.InputField();
        pubPanel = new javax.swing.JPanel();
        pubComboView = new gui.components.ComboView();
        costPanel = new javax.swing.JPanel();
        costField = new gui.components.InputField();
        jScrollPane2 = new javax.swing.JScrollPane();
        authorListView = new gui.components.ListView();
        addAuthorBtn = new javax.swing.JButton();
        deleteAuthorBtn = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        genreListView = new gui.components.ListView();
        addGenreBtn = new javax.swing.JButton();
        deleteGenreBtn = new javax.swing.JButton();
        genreComboView = new gui.components.ComboView();
        authorComboView = new gui.components.ComboView();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        CommonBtn.setBackground(C_BACKGROUND);
        CommonBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        CommonBtn.setText("Отмена");
        CommonBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommonBtnActionPerformed(evt);
            }
        });

        SuccessBtn.setBackground(C_GREEN);
        SuccessBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SuccessBtn.setForeground(C_GRAY);
        SuccessBtn.setText("Сохранить");
        SuccessBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SuccessBtnActionPerformed(evt);
            }
        });

        namePanel.setBackground(C_BACKGROUND);
        namePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Название:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        nameField.setBackground(C_GRAY);
        nameField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout namePanelLayout = new javax.swing.GroupLayout(namePanel);
        namePanel.setLayout(namePanelLayout);
        namePanelLayout.setHorizontalGroup(
            namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nameField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        namePanelLayout.setVerticalGroup(
            namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        descPanel.setBackground(C_BACKGROUND);
        descPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Описание:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        descField.setBackground(C_GRAY);
        descField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout descPanelLayout = new javax.swing.GroupLayout(descPanel);
        descPanel.setLayout(descPanelLayout);
        descPanelLayout.setHorizontalGroup(
            descPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(descField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        descPanelLayout.setVerticalGroup(
            descPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(descField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        ISBNPanel.setBackground(C_BACKGROUND);
        ISBNPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ISBN:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        ISBNField.setBackground(C_GRAY);
        ISBNField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout ISBNPanelLayout = new javax.swing.GroupLayout(ISBNPanel);
        ISBNPanel.setLayout(ISBNPanelLayout);
        ISBNPanelLayout.setHorizontalGroup(
            ISBNPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ISBNField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        ISBNPanelLayout.setVerticalGroup(
            ISBNPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ISBNField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pubPanel.setBackground(C_BACKGROUND);
        pubPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Издатель:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        pubComboView.setBackground(C_BACKGROUND);
        pubComboView.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout pubPanelLayout = new javax.swing.GroupLayout(pubPanel);
        pubPanel.setLayout(pubPanelLayout);
        pubPanelLayout.setHorizontalGroup(
            pubPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pubComboView, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pubPanelLayout.setVerticalGroup(
            pubPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pubComboView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        costPanel.setBackground(C_BACKGROUND);
        costPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Стоимость:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        costField.setBackground(C_GRAY);
        costField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout costPanelLayout = new javax.swing.GroupLayout(costPanel);
        costPanel.setLayout(costPanelLayout);
        costPanelLayout.setHorizontalGroup(
            costPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(costField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        costPanelLayout.setVerticalGroup(
            costPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(costField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        authorListView.setBackground(C_BACKGROUND);
        authorListView.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        authorListView.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                authorListViewValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(authorListView);

        addAuthorBtn.setBackground(C_BLUE);
        addAuthorBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        addAuthorBtn.setForeground(C_GRAY);
        addAuthorBtn.setText("Добавить");
        addAuthorBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAuthorBtnActionPerformed(evt);
            }
        });

        deleteAuthorBtn.setBackground(C_RED);
        deleteAuthorBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        deleteAuthorBtn.setForeground(C_GRAY);
        deleteAuthorBtn.setText("Удалить");
        deleteAuthorBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteAuthorBtnActionPerformed(evt);
            }
        });

        genreListView.setBackground(C_BACKGROUND);
        genreListView.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        genreListView.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                genreListViewValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(genreListView);

        addGenreBtn.setBackground(C_BLUE);
        addGenreBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        addGenreBtn.setForeground(C_GRAY);
        addGenreBtn.setText("Добавить");
        addGenreBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addGenreBtnActionPerformed(evt);
            }
        });

        deleteGenreBtn.setBackground(C_RED);
        deleteGenreBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        deleteGenreBtn.setForeground(C_GRAY);
        deleteGenreBtn.setText("Удалить");
        deleteGenreBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteGenreBtnActionPerformed(evt);
            }
        });

        genreComboView.setBackground(C_BACKGROUND);
        genreComboView.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        authorComboView.setBackground(C_BACKGROUND);
        authorComboView.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(namePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(SuccessBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CommonBtn))
                    .addComponent(descPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ISBNPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pubPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(costPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 642, Short.MAX_VALUE)
                    .addComponent(jScrollPane3)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(authorComboView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(addAuthorBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteAuthorBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(genreComboView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addGenreBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteGenreBtn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(namePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(descPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ISBNPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pubPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(costPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addAuthorBtn)
                    .addComponent(deleteAuthorBtn)
                    .addComponent(authorComboView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addGenreBtn)
                    .addComponent(deleteGenreBtn)
                    .addComponent(genreComboView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SuccessBtn)
                    .addComponent(CommonBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CommonBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CommonBtnActionPerformed
        dispose();
    }//GEN-LAST:event_CommonBtnActionPerformed

    private void SuccessBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SuccessBtnActionPerformed
        nameField.required();
        ISBNField.required();
        costField.required();
        
        float cost = costField.getFloat();
        
        book.setName(nameField.getText());
        book.setDescription(descField.getText());
        book.setIsbn(ISBNField.getText());
        book.setCost(cost);
        book.setPublisher((Publisher) pubComboView.getSelectedItem());
        
        commit();
    }//GEN-LAST:event_SuccessBtnActionPerformed

    private void authorListViewValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_authorListViewValueChanged
        deleteAuthorBtn.setEnabled(true);
    }//GEN-LAST:event_authorListViewValueChanged

    private void addAuthorBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAuthorBtnActionPerformed
        Author author = (Author) authorComboView.getSelectedItem();
        
        book.getAuthors().add(author);
        
        updateUI();
    }//GEN-LAST:event_addAuthorBtnActionPerformed

    private void deleteAuthorBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteAuthorBtnActionPerformed
        Author author = (Author) authorListView.getSelectedValue();

        if (!confirm("Вы действительно хотите удалить автора?")) {
            return;
        }
        
        book.getAuthors().remove(author);
        
        updateUI();
    }//GEN-LAST:event_deleteAuthorBtnActionPerformed

    private void genreListViewValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_genreListViewValueChanged
        deleteGenreBtn.setEnabled(true);
    }//GEN-LAST:event_genreListViewValueChanged

    private void deleteGenreBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteGenreBtnActionPerformed
        Genre genre = (Genre) genreListView.getSelectedValue();

        if (!confirm("Вы действительно хотите удалить жанр?")) {
            return;
        }
        
        book.getGenres().remove(genre);
        
        updateUI();
    }//GEN-LAST:event_deleteGenreBtnActionPerformed

    private void addGenreBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addGenreBtnActionPerformed
        Genre genre = (Genre) genreComboView.getSelectedItem();
        
        book.getGenres().add(genre);
        
        updateUI();
    }//GEN-LAST:event_addGenreBtnActionPerformed

    @Override
    protected void afterInit() {
        setLocationRelativeTo(null);
        getContentPane().setBackground(C_BACKGROUND);
        
        pubComboView.load(Publisher.class.getSimpleName());
        
        nameField.setText(book.getName());
        descField.setText(book.getDescription());
        ISBNField.setText(book.getIsbn());
        costField.setFloat(book.getCost());
        
        pubComboView.setSelectedItem(book.getPublisher());
        
        updateUI();
    }
    
    private void updateUI() {
        genres = (ArrayList<Genre>) getEntities(Genre.class.getSimpleName());
        authors = (ArrayList<Author>) getEntities(Author.class.getSimpleName());
        
        ArrayList<Author> curAuthors = new ArrayList<>();
        ArrayList<Genre> curGenres = new ArrayList<>();
        
        for (Genre genre : book.getGenres()) {
            genres.remove(genre);
            curGenres.add(genre);
        }
        for (Author author : book.getAuthors()) {
            authors.remove(author);
            curAuthors.add(author);
        }
        authorComboView.load(authors);
        genreComboView.load(genres);
        
        authorListView.load(curAuthors);
        genreListView.load(curGenres);
        
        deleteGenreBtn.setEnabled(false);
        deleteAuthorBtn.setEnabled(false);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CommonBtn;
    private gui.components.InputField ISBNField;
    private javax.swing.JPanel ISBNPanel;
    private javax.swing.JButton SuccessBtn;
    private javax.swing.JButton addAuthorBtn;
    private javax.swing.JButton addGenreBtn;
    private gui.components.ComboView authorComboView;
    private gui.components.ListView authorListView;
    private gui.components.InputField costField;
    private javax.swing.JPanel costPanel;
    private javax.swing.JButton deleteAuthorBtn;
    private javax.swing.JButton deleteGenreBtn;
    private gui.components.InputField descField;
    private javax.swing.JPanel descPanel;
    private gui.components.ComboView genreComboView;
    private gui.components.ListView genreListView;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private gui.components.InputField nameField;
    private javax.swing.JPanel namePanel;
    private gui.components.ComboView pubComboView;
    private javax.swing.JPanel pubPanel;
    // End of variables declaration//GEN-END:variables
}
