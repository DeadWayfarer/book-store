package gui.dialogs;

import entities.Publisher;
import gui.base.BaseDialog;
import static main.Main.*;
import utils.HibernateUtil;
import static utils.HibernateUtil.*;
import static utils.MessageUtil.*;
import static utils.Style.*;

/**
 * Диалог для редактирования сущности Publisher
 * @author Teacher
 */
public class AddEditPublisherDialog extends BaseDialog {

    private Publisher publisher;
    
    public AddEditPublisherDialog(Publisher publisher) {
        super();
        initComponents();
        this.publisher = publisher;
        afterInit();
    }

    /**
     * Сгенерированный контент
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CommonBtn = new javax.swing.JButton();
        SuccessBtn = new javax.swing.JButton();
        namePanel = new javax.swing.JPanel();
        nameField = new gui.components.InputField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        CommonBtn.setBackground(C_BACKGROUND);
        CommonBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        CommonBtn.setText("Отмена");
        CommonBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommonBtnActionPerformed(evt);
            }
        });

        SuccessBtn.setBackground(C_GREEN);
        SuccessBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SuccessBtn.setForeground(C_GRAY);
        SuccessBtn.setText("Сохранить");
        SuccessBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SuccessBtnActionPerformed(evt);
            }
        });

        namePanel.setBackground(C_BACKGROUND);
        namePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Текст:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        nameField.setBackground(C_GRAY);
        nameField.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout namePanelLayout = new javax.swing.GroupLayout(namePanel);
        namePanel.setLayout(namePanelLayout);
        namePanelLayout.setHorizontalGroup(
            namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nameField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        namePanelLayout.setVerticalGroup(
            namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(namePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(SuccessBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 156, Short.MAX_VALUE)
                        .addComponent(CommonBtn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(namePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SuccessBtn)
                    .addComponent(CommonBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CommonBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CommonBtnActionPerformed
        dispose();
    }//GEN-LAST:event_CommonBtnActionPerformed

    private void SuccessBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SuccessBtnActionPerformed
        nameField.required();
        
        publisher.setName(nameField.getText());
        
        commit();
    }//GEN-LAST:event_SuccessBtnActionPerformed

    @Override
    protected void afterInit() {
        setLocationRelativeTo(null);
        getContentPane().setBackground(C_BACKGROUND);
        
        nameField.setText(publisher.getName());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CommonBtn;
    private javax.swing.JButton SuccessBtn;
    private gui.components.InputField nameField;
    private javax.swing.JPanel namePanel;
    // End of variables declaration//GEN-END:variables
}
