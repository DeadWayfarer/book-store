package gui.dialogs;

import gui.base.BaseDialog;
import static main.Main.*;
import utils.HibernateUtil;
import static utils.HibernateUtil.*;
import static utils.MessageUtil.*;
import static utils.Style.*;

/**
 * Диалог для редактирования сущности 
 * @author Teacher
 */
public class EmptyDialog extends BaseDialog {

    public EmptyDialog() {
        super();
        initComponents();
        afterInit();
    }

    /**
     * Сгенерированный контент
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CommonBtn = new javax.swing.JButton();
        SuccessBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        CommonBtn.setBackground(C_BACKGROUND);
        CommonBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        CommonBtn.setText("Отмена");
        CommonBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommonBtnActionPerformed(evt);
            }
        });

        SuccessBtn.setBackground(C_GREEN);
        SuccessBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SuccessBtn.setForeground(C_GRAY);
        SuccessBtn.setText("Сохранить");
        SuccessBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SuccessBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(SuccessBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 156, Short.MAX_VALUE)
                .addComponent(CommonBtn)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(264, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SuccessBtn)
                    .addComponent(CommonBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CommonBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CommonBtnActionPerformed
        dispose();
    }//GEN-LAST:event_CommonBtnActionPerformed

    private void SuccessBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SuccessBtnActionPerformed

        commit();
    }//GEN-LAST:event_SuccessBtnActionPerformed

    @Override
    protected void afterInit() {
        setLocationRelativeTo(null);
        getContentPane().setBackground(C_BACKGROUND);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CommonBtn;
    private javax.swing.JButton SuccessBtn;
    // End of variables declaration//GEN-END:variables
}
